\documentclass[usenames,dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}

\usepackage[
	backend=biber,
	style=chem-angew,
	citestyle=phys,
	sorting=ynt
	]{biblatex}
\graphicspath{
    {images/}
    {../images/}
    }

\addbibresource{biblio.bib} %Imports bibliography file
\setbeamertemplate{bibliography item}{\insertbiblabel}
\renewcommand*{\bibfont}{\normalfont\tiny}

%\setbeamertemplate{bibliography entry article}{}
%\setbeamertemplate{bibliography entry title}{}
%\setbeamertemplate{bibliography entry location}{}
%\setbeamertemplate{bibliography entry note}{}

%\usepackage{header}
%\usetheme{AnnArbor}
\usetheme[width=60pt]{Berkeley}
%\usecolortheme{whale}
%\usecolortheme{seahorse}
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{items}[ball]
%\definecolor{UniBlue}{RGB}{83,121,170}
%\setbeamercolor{title}{fg=UniBlue}
%\setbeamercolor{frametitle}{fg=UniBlue}
%\setbeamercolor{structure}{fg=UniBlue}
%\setbeamercolor{title}{bg=red!65!black, fg=white}

\makeatletter
\setlength{\beamer@headheight}{40pt}
%\setlength{\beamer@sidebarwidth}{200pt}
\makeatother

\title{Ising model in fractional dimensions}
\author{Marco Malandrone}
\institute[]{\textbf{Scuola Normale Superiore\\ \vspace{10pt} Advisor: Prof. Massimo D'Elia}}
\date{\today}

\newcommand\blfootnote[1]{%
	\begingroup
	\renewcommand\thefootnote{}\footnote{#1}%
	\addtocounter{footnote}{-1}%
	\endgroup
}
\newcommand{\dd}{\mathrm{d}}

\begin{document}

\frame{
	\titlepage 
	\vspace{0pt}\blfootnote{\vspace{7pt}\tiny Sources and pdfs at
	\url{https://gitlab.com/1998marcom/fractional-ising-latex}}
	\nocite{zero}\nocite{first}\nocite{second}\nocite{third}
	\nocite{axiomaticspaces}\nocite{axiomaticspaces2}
	\nocite{data1}\nocite{data2}\nocite{data3}\nocite{data4}
	}

\section{What and how: definitions}
\subsection{Generalities}
\begin{frame}
	\frametitle{Generalities}
	\framesubtitle{What does a non-integer dimension mean?\cite{mandelbrot}}
	\begin{block}{Dimensionality definition}
		\[V\propto R^d\]
	\end{block}
	\begin{block}{Issues}
		\begin{itemize}
			\item How to measure distances? 
			\item How to measure the $d-$volume?
		\end{itemize}
	\end{block}
	\begin{exampleblock}{Possibile solutions}
		\begin{itemize}
			\item We need a metric space with no defined dimension a priori.
			\item If we allow for a discrete space, a graph would do the job.
		\end{itemize}
	\end{exampleblock}

\end{frame}
\begin{frame}
	\frametitle{Generalities}
	\framesubtitle{Defining a graph dimension}
	\begin{columns}
		\centering
		\begin{column}{0.55\textwidth}
			\begin{block}{Distance}
				The \textit{graph distance} between two vertices $A$ and $B$:
				\vspace{3pt}
				
				the minimum number of edges to cross to get from $A$ to $B$.
			\end{block}
		\end{column}
		\begin{column}{0.40\textwidth}
			\begin{block}{Volume}
				Volume of a set $V(\mathcal{S})$: 
				\vspace{3pt}
				
				the number of vertices belonging to the set $\mathcal{S}$.
			\end{block}
		\end{column}
	\end{columns}
	\begin{block}{Graph dimension}
		\[d_S = \lim_{R/a\rightarrow\infty} \;\frac{\log\left(V(B_R)\right)}{\log R}\]
		where $B_R$ is the ball of radius $R$
	\end{block}
\end{frame}
\subsection{Translational symmetry}
\begin{frame}
	\frametitle{Dimension definition and translations}
	\begin{block}{}
		\begin{itemize}
			\item Traditional lattices have a translational symmetry.
			\item In order to define the dimension, translational symmetry is not necessary.
			\item There exist oject such as fractals which have fractional dimension but not necessarily
				translational symmetry.
		\end{itemize}
	\end{block}
	\begin{figure}
		\includegraphics[width=0.45\textwidth]{translatedcarpet.png}
		\caption{A fractal \textit{usually} has no translational symmetries}
	\end{figure}
\end{frame}
\subsection{Fractals and fractal dimensions}
\begin{frame}
	\frametitle{Fractals}
	\framesubtitle{Fractal dimension}
	\begin{exampleblock}{}
		Let's then build a graph from a fractal
	\end{exampleblock}
	\begin{block}{Fractal dimensions}
		\begin{itemize}
			\item Various definitions
			\item One of the most intuitive is the Minkowski or \textit{box-counting} one:
		\end{itemize}
		\begin{equation*}
		        \mathrm{dim_{box}}(S) := 
	       		\lim_{\varepsilon\rightarrow0}
		        \frac{\log \left(N_{C_{\large \varepsilon}}(S)\right)}{\log(1/\varepsilon)}
		\end{equation*}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Fractals}
	\framesubtitle{Constructing a graph from a fractal}
	\centering
	\begin{block}{Build a finite fractal}
		\centering
		\includegraphics[width=0.7\textwidth]{generator.png}
	\end{block}
	\begin{columns}
		\begin{column}{0.3\textwidth}
	\includegraphics[width=0.9\textwidth]{carpet2.png}
		\end{column}
		\begin{column}{0.38\textwidth}
			\begin{block}{}
				\[\longrightarrow\]
				\centering
				Replace black squares with vertices.
			\end{block}
			\hspace{5pt}
		\end{column}
		\begin{column}{0.3\textwidth}
			\hspace{5pt}
	\includegraphics[width=0.9\textwidth]{carpet2ising.png}
		\end{column}
	\end{columns}
\end{frame}
\setbeamerfont{caption}{size=\tiny}
\begin{frame}
	\frametitle{Fractals}
	\framesubtitle{Graph dimension and fractal dimension}
	\begin{alertblock}{}
		Fractal dimension $d_H$ $=$ $d_S$ graph dimension?
	\end{alertblock}
	\begin{block}{They may}
		A sufficient condition is the graph distance and the environment $p-$distances being 
		\textit{strongly equivalent}:
		\[m\,d_p(A,B) \le d_G(A,B) \le M\,d_p(A,B)\]
	\end{block}
	\begin{columns}
		\begin{column}{0.7\textwidth}
			\begin{block}{}
				Analogously:
				\[m\,d_p^g(A,B) \le d_G(A,B) \le M\,d_p^g(A,B) \Rightarrow\]
				\[\Rightarrow d_S = \frac1gd_H\]
			\end{block}
		\end{column}
		\begin{column}{0.28\textwidth}
				\tiny
			\begin{figure}
				\centering
				\includegraphics[width=0.7\textwidth]{pathologicaldistance.png}
				\caption{Pathological generator with $g=\log8/\log6$}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}
\setbeamerfont{caption}{size=\footnotesize}
%\subsection{Alternative spaces} VA BENE ANCHE SOLO UNA MENZIONE A VOCE
%\begin{frame}
%	\frametitle{Alternatives}
%	\framesubtitle{Other spaces with non-integer dimensions}
%	\begin{exampleblock}{}
%	There are other attempts to define a metric space (even a continuous one), 
%	in works such as the ones from \textcite{axiomaticspaces} 
%	and \textcite{axiomaticspaces2}.
%	\end{exampleblock}
%	aggiungere qualcosa
%\end{frame}

\section{Renormalization group}
\subsection{Real-space renormalization}
\begin{frame}{}
	\frametitle{Real-space renormalization}
	\framesubtitle{\textit{Blocking} operation on the Ising model}
	\begin{columns}
		\centering
		\begin{column}{0.66\textwidth}
			\begin{block}{Why}
				$\rightarrow$ Scale invariance at critical point
			\end{block}
			\begin{block}{Blocking operation}
				\[Z = \mathrm{Tr}_s e^{-\mathcal{H}(s)} = \mathrm{Tr}_{s'} e^{-\mathcal{H'}(s')}\]

				\centering
				$\mathcal{H}:\Sigma\rightarrow \mathbb{R}$
				\hspace{10pt}
				$\longrightarrow$
				\hspace{10pt}
				$\mathcal{H'}:\Sigma\rightarrow \mathbb{R}$

			\end{block}
		\end{column}
		\begin{column}{0.3\textwidth}
			\includegraphics[width=0.96\textwidth]{blockspin.png}
		\end{column}
	\end{columns}
	\begin{block}{The coefficients space}
		\begin{itemize}
			\item $\mathcal{H} \leftrightarrow \{ K_i \} $
			\item $ \{ K'_i \} = \mathcal{R}\left(\{ K_i \} \right)$
		\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Real-space renormalization}
	\framesubtitle{Relevant, marginal and irrelevant}
	\begin{block}{Around a fixed point $\{K^*_i\}$}
		\[K'_i-K_i^* \approx \sum_j M_{ij} (K_j - K^*_j)\]
		\[ u'_i = \lambda_i u_i = b^{y_i}u_i \]
	\end{block}
	\begin{block}{Relevant, marginal, irrelevant}
		\begin{itemize}
			\item Relevant: $y_i > 0$
			\item Marginal: $y_i = 0$
			\item Irrelevant: $y_i < 0$
		\end{itemize}
	\end{block}
	\begin{exampleblock}{Ising model: two relevant eigenvalues}
		\[y_t \quad\quad y_h\]
	\end{exampleblock}
\end{frame}
\begin{frame}
	\frametitle{Real-space renormalization}
	\framesubtitle{The usual critical exponents: $\nu$ and $\theta$}
	\begin{block}{Definitions}
		\begin{itemize}
			\item $t = \frac{T-T_c}{T_c}$
			\item $\xi \propto |t|^{-\nu}$ \hspace{8pt}@$B=0$

			\item $\xi \propto |B|^{-\theta}$ \hspace{8pt}@$T=T_c$
		\end{itemize}
	\end{block}
	\begin{block}{In terms of $y_t$ and $y_h$}
		From the scaling behaviour of the correlation function: \\
		\begin{itemize}
			\item $\nu = 1/y_t$ 
			\item $\theta = 1/y_h$
		\end{itemize}
	\end{block}

\end{frame}

\subsection{Scaling relations}
\begin{frame}
	\frametitle{Scaling relations}
	\framesubtitle{The other critical exponents}
	From the singular part of the free energy scaling:
	\[f_s(\{u_i\}) = b^{-d}f_s(\{\lambda_i u_i\})\]
	The scale invariance is sufficient to obtain:
	\begin{align*}
		c&\sim |t|^{-\alpha} &&\alpha = 2 - \nu d \\
		m&\sim |t|^\beta &&\beta  = \nu \left[ d - \frac1\theta\right] \\
		\chi&\sim |t|^{-\gamma} &&\gamma = \nu \left[\frac2\theta - d \right] \\
		m&\sim |B|^{1/\delta} \,\mathrm{sign}(B) &&\delta = \frac1{d\theta-1}
	\end{align*}
\end{frame}

\subsection{MCRG}
\begin{frame}
	\frametitle{Monte Carlo Renormalization Group}
	\framesubtitle{How to measure the scaling eigenvalues $y_t$ and $y_h$?}
	\begin{block}{In the neighbourhood of the fixed point $\{K^*_i\}$}
		\vspace{-2pt}
		\[\{K_i\} \,\leftrightarrow \,\{u_i\} = \{t, h, 0, 0, \dots, 0\}\]
		\[\mathcal H'[t, h] = \mathcal R^n\left(\mathcal{H}[t, h]\right) \;\simeq\; 
		\mathcal H[b^{ny_t}t, b^{ny_h}h]\]
		\vspace{-10pt}

	\end{block}
	\begin{block}{Approximatively, with $B=0$}
		\begin{itemize}
			%\item We can truncate the hamiltonians and take only the most important terms (e.g.: the neighbours coupling)
			\item $t = \frac{T-T_c}{T_c}$ is the only relevant scaling variable
		\end{itemize}
		\[
			\mathcal{H} = K_1(t)\,\Sigma_{i, j} \sigma_i \sigma_j \quad\longrightarrow\quad
			\mathcal{H'} = K_1(t')\,\Sigma_{i, j} \sigma_i' \sigma_j'
		\]

	\end{block}
	\begin{block}{Renormalized system temperature}
		\[  u'(t) \approx u(t') \;\; \Rightarrow \;\; T'\approx u^{-1}\left(u'(T)\right)\]
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Monte Carlo Renormalization Group}
	\framesubtitle{How to measure the critical exponents}
	\begin{block}{Rescaling the correlation length $\xi$}
		\[\xi' = \xi/b \; \Rightarrow \; \xi/\xi' = b\]
	\end{block}
	\begin{block}{$\nu$ definition}
		\[\xi \propto |t|^{-\nu} \; \Rightarrow \; \xi/\xi' = (|t|/|t'|)^{-\nu}\]
	\end{block}
	\begin{block}{What to measure}
		\[\nu = \frac{\log b}{\log\frac{\dd T'}{\dd T}}
		= \frac{\log b}{\log\left(\frac{\dd u'}{\dd T}\Big/\frac{\dd u}{\dd T}\right)}\]
	\end{block}
\end{frame}

\section{Fractals}
\subsection{Criticality conditions}
\begin{frame}
	\frametitle{Phase transitions on fractals}
	\framesubtitle{Conditions for phase transition}
	\begin{alertblock}{Condition for $T_c > 0$}
		Q: under which conditions there is a phase transition at $T_c > 0$?
	\end{alertblock}
	\begin{block}{A preliminary definition\cite{mandelbrot}}
		RAMIFICATION ORDER $\mathcal R_P$ of a point $P$: the least number of points to remove
		so as to cut away a neighbourhood of $P$ from the rest of the fractal.
		\vspace{5pt}

		There is a minimum $\mathcal R_\mathrm{min}$ and a maximum $\mathcal R_\mathrm{max}$.
		\vspace{5pt}

		Urysohn showed that $\mathcal R_\mathrm{max} \ge 2\,\mathcal R_\mathrm{min} - 2$
	\end{block}
	\begin{exampleblock}{Solution}
		\[T_c > 0 \Leftrightarrow \mathcal R_\mathrm{min} = \infty \]
	\end{exampleblock}
\end{frame}
\begin{frame}
	\frametitle{Phase transitions on fractals}
	\framesubtitle{Making Peierls \textit{droplets}}
	\begin{block}{Energy fluctuations}
		\[\Delta F = \Delta U - T \Delta S \]

		They depend only on what happens on the perimeter of the droplet.
	\end{block}
	\begin{block}{For a droplet}
		In a proper infinite fractal,
		whatever neighbourhood, of any point $P$, contains an infinite number of vertices.
		
		If there is a point $P$ such that $\mathcal R_P < \infty$, then we can cut away an
		infinite set, with infinite energy, and only need a finite energy fluctuation to flip
		the droplet.
	\end{block}


\end{frame}
\begin{frame}
	\frametitle{Phase transitions on fractals}
	\framesubtitle{Noteworthy consequences}
	\begin{itemize}
		\item One can create a graph with an arbitrarely high dimension $d_G$, 
			while having no phase transition for $T > 0$. \vspace{5pt}
		\item One can crate a graph with a dimension $d_G$ arbitrarely close to 1,
			and still have a phase transition with $T_c > 0$. \vspace{5pt}
		\item There is then no \textit{lower critical dimension} if one requires the sole
			scale-invariance.
	\end{itemize}
\end{frame}

\subsection{Sierpinski's carpets}
%\begin{frame}
%	\frametitle{Sierpinski's carpets}
%	\framesubtitle{Why they are nice}
%\end{frame}
\begin{frame}
	\frametitle{Sierpinski's carpets}
	\framesubtitle{Some preliminary definitions}
	\begin{itemize}
		\item They exhibit a phase transition at $T_c > 0$
		\item The graph distance is strongly equivalent with $p-$distances, hence $d_H = d_S$.
	\end{itemize}
	\begin{alertblock}{}
		In order to define a generator, the mere dimensionality $d_H$ is not enough.
	\end{alertblock}
	\begin{block}{Other common parameters}
	\begin{itemize}
		\item CONNECTIVITY $\mathcal Q$\cite{mandelbrot}: the minimum dimension of a frontier
			of neighbourhood of a point.
		\item LACUNARITY $\mathcal L$: 
			a measure of the ``mean size'' of gaps in a fractal, hence also of
			how much the fractals fails at being translationally invariant.
	\end{itemize}
	\end{block}
\end{frame}
\begin{frame}
	\frametitle{Sierpinski's carpets}
	\framesubtitle{My simulations}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{block}{Simulation parameters}
				\begin{itemize}
					\item $d_\mathrm{env}$: the environment dimension,
					\item $b$: the length of the edge of the generator,
					\item $s$: the scale (or iteration number) of the fractal,
					\item $e$: extra number of occupied subsquare in a generator.
				\end{itemize}
			\end{block}
		\end{column}
		\begin{column}{0.46\textwidth}
			\begin{figure}
				\centering
				\includegraphics[width=0.96\textwidth]{examplesimulation.png}
				\caption{Generator with $d_\mathrm{env}=2, \,b=5, \,e=2$}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}
\subsection{Simulations results}
\begin{frame}
	\frametitle{Simulations results}
	\framesubtitle{Dimensionality effects on $\beta_C$}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{beta_dim.png}
		\caption{My simulation results}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Simulations results}
	\framesubtitle{Dimensionality effects on $\nu$}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{nu_dim.png}
		\caption{My simulation results}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Simulations results}
	\framesubtitle{Different topologies}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{topology_data3.jpg}
		\caption{Carpets generators chosen by \textcite{data3}}
	\end{figure}
	\begin{figure}
		\includegraphics[width=\textwidth]{reducedtable_data3.jpg}
		\caption{Results from \textcite{data3}}
	\end{figure}
\end{frame}
%\begin{frame}
%	\frametitle{Simulations results}
%	\framesubtitle{Results from \textcite{data3}}
%\end{frame}
% Importante! Metti le mie, quelle degli altri 

\subsection{Universality and CFT}
\begin{frame}
	\frametitle{Universality and CFT}
	\framesubtitle{Simulation results $\neq$ results from $\varepsilon-$expansion}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{data3_nu.jpg}
		\caption{Results from \textcite{data3} compared with $\varepsilon-$expansion results\cite{data3}}
	\end{figure}
\end{frame}
% Spiega che l'invarianza traslazionale serve a definire la classe di universalità

\subsection{Translationally symmetric fractals}
\begin{frame}
	\frametitle{Building a translationally symmetric fractal}

	\begin{alertblock}{}
		Do translationally symmetric (nonlacunar) fractals exist?
	\end{alertblock}
	\begin{block}{Yes\cite{mandelbrot}, but \dots}
		\begin{itemize}
			\item They are everywhere dense.
			\item They are nonclosed, otherwise they would coincide with $R^N$, 
				the environment space.
			\item Their box-counting dimension is $N$, while they can have a non integer
				Hausdorff dimension.
		\end{itemize}
	\end{block}
	%Given the definitions of lacunarity in Chapter
	%34, a nonlacunar set in the space $\mathbb{R}^E$ should
	%intersect every cube or sphere in said space.
	%In mathematical terms, it should be 
	%everywhere dense, hence nonclosed. (The only 
	%everywhere dense closed set is $\mathbb{R}^E$ itself!) This
	%entry shows that such fractals do exist, but
	%"feel" very different from the closed fractals
	%in the rest of this Essay. A key symptom is
	%that the Hausdorff Besicovitch dimension 
	%remains workable, but the similarity and 
	%Minkowski Bouligand dimensions are equal to $E$,
	%rather than to the Hausdorff Besicovitch $D$.
\end{frame}
\subsection{Scaling relations}

\begin{frame}
	\frametitle{Scaling relations}
	\framesubtitle{They are verified}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{dim_dim.png}
		\caption{Results from my simulations}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Scaling relations}
	\framesubtitle{They are verified}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{data3_dim.jpg}
		\caption{Results from\cite{data3}}
	\end{figure}
\end{frame}

\begin{frame}{References}
	\printbibliography
\end{frame}
\end{document}
