# Fractional Ising Latex

Repository per i latex del colloquio: presentazione e appunti. [Qua](https://gitlab.com/1998marcom/fractional-ising) il repo dei conti.

[Qui](https://gitlab.com/1998marcom/fractional-ising-latex/-/jobs/artifacts/master/raw/slides/slidesColloquioMalandrone.pdf?job=build) la presentazione. [Qui](https://gitlab.com/1998marcom/fractional-ising-latex/-/jobs/artifacts/master/raw/appunti/appuntiColloquioMalandrone.pdf?job=build) gli appunti.
