\documentclass[a4paper, 10pt]{article}
\DeclareMathSizes{10}{11}{9}{8}
\usepackage[top=3cm, bottom=3cm, inner=3cm, outer=3cm]{geometry}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\newcommand{\dd}{\mathrm{d}}

\title{Ising model in fractional dimensions\footnote{\vspace{3pt} Sorgenti e pdf su \url{https://gitlab.com/1998marcom/fractional-ising-latex}}}
\author{Marco Malandrone}
%\author{Marco Malandrone\\\textbf{Scuola Normale Superiore\\ \vspace{10pt} Relatore: Prof. Massimo D'Elia}}

\date{\today}

\newcommand{\slidesep}{\vspace{10pt}\hline\vspace{6pt}}

\begin{document}

\maketitle



\section{Cosa e come: definizioni}
\subsection{In generale}

Partiamo subito da cosa vuol dire definire una dimensione non intera: 
in generale, ci aspettiamo che il $d$-volume di una palla di raggio $R$ in uno spazio metrico 
(non vettoriale sicuramente, ma mi serve qualcosa in più di metrico) 
di dimensione $d$ sia proporzionale a $R^d$.
Sorgono quindi due domande:
\begin{enumerate}
	\item Come misuriamo le distanze? Quali metriche vanno bene per il nostro spazio?
	\item Come misuriamo il $d$-volume?
\end{enumerate}
Si può trovare una soluzione semplice se ammettiamo uno spazio
discreto: il nostro sistema è allora rappresentato da un grafo,
con da vertici discreti e un insieme di archi che definiscono 
una relazione di vicinanza fra i vertici.

\slidesep

In particolare:
\begin{enumerate}
	\item Come distanza fra i punti possiamo prendere il concetto di distanza dalla teoria dei grafi, 
		ovvero definire la distanza fra due vertici A e B
		come il minimo numero di archi da attraversare per giungere da A a B. 
	\item Come volume di un insieme possiamo prendere 
		il numero di vertici appartenenti all'insieme.
\end{enumerate}
Dalla definizione di distanza, segue la definizione delle palle.
Abbiamo a questo punto tutti gli strumenti per definire una dimensione $d_S$ a partire dal volume della palla di raggio $R$.

\subsection{La simmetria traslazionale}
Nel caso di una dimensione intera $d$ possiamo scegliere come grafo un reticolo $d$-dimensionale. 
Questo grafo esibisce però una simmetria addizionale che noi non abbiamo richiesto prima, 
ovvero la simmetria di traslazione. 

In generale però la simmetria di traslazione, 
non è necessaria per definire semplicemente la dimensione di un sistema: 
per questo scopo è sufficiente la simmetria di scala. 

In particolare esistono oggetti come i frattali che hanno una dimensione non intera e non hanno simmetria di traslazione.

\subsection{I frattali: dimensione frattale}
Rinunciando alla simmetria traslazionale, possiamo scegliere di prendere un grafo 
costruito su un frattale. 
La simmetria di scala è così garantita dalla \textit{self-similarity} del frattale. 

Abbiamo inoltre diverse definizioni per la dimensione $d_H$ del frattale su cui costruiamo il grafo. 
Salvo casi patologici, queste coincidono. Una delle più intuitive è la dimensione di Minkowski, nota 
anche come dimensione di \textit{box-counting}. 
È definita a partire dal limite di iper-cubi di lato $\varepsilon$ 
necessari a ricoprire il frattale nel limite in cui $\varepsilon \rightarrow 0$:
\begin{equation}
	\mathrm{dim_{box}}(S) := 
	\lim_{\varepsilon\rightarrow0}
	\frac{\log \left(N_{B_{\large \varepsilon}}(S)\right)}{\log(1/\varepsilon)}
\end{equation}

\slidesep

Occorre però specificare come intendiamo costruire un grafo a partire da un frattale.

Limitiamoci al caso dei frattali definiti con una regola ricorsiva.
Definendo come \textit{generatore} di un frattale la sua prima iterazione, 
restringiamoci ulteriormente al caso in cui questo \textit{generatore} è un cubo di dimensione ambiente 
$d_{\mathrm{env}}$, suddiviso in $b^d$ $d_{\mathrm{env}}$-cubetti.
Di questi $b^d$ cubetti, ne scegliamo $N$ da riempire iterativamente e ne lasciamo vuoti $b^d-N$.

Ripetiamo questo procedimento ricorsivamente per $s$ volte. Alla fine otteniamo un insieme di cubetti 
selezionati con una relazione di adiacenza fra di loro. Possiamo allora scegliere i cubetti come 
vertici del nostro grafo, e collegarli proprio seguendo le relazioni di adiacenza che già esistono.

Nel limite in cui $s\gg1$ otteniamo un grafo che è anche
buona descrizione (anche se discretizzata) di uno spazio con una dimensione di scala $d_S$ 
non necessariamente intera. 

\slidesep

Sorge a questo punto una domanda: la dimensione $d_H$ del frattale coincide con quella $d_S$ del grafo?


Si può mostrare\footnote{Ovvero devo scrivere in \LaTeX} che la dimensione del grafo 
coincide con quella del frattale se la distanza del grafo è fortemente equivalente a 
quelle $p$-esime dello spazio ambiente (e si può dire anche quanto vale la dimensione del grafo se la 
distanza del grafo è fortemente equivalente a una potenza $j$-esima delle distanze $p$-esime 
dello spazio ambiente).

%\subsection{Alternative}
%Questo modo di procedere è in realtà solo una delle alternative possibili. Altre alternative sono 
%presentate in paper come \href{https://www.princeton.edu/~fhs/fhspapers/fhspaper91.pdf}{questo} e 
%nell'appendice di \href{https://journals.aps.org/prd/pdf/10.1103/PhysRevD.7.2911}{questo}.
%
%VALUTARE SE ESPANDERE O NO SU QUESTE DEFINIZIONI ALTERNATIVE, MAGARI ENUNCIANDO SOLO I POSTULATI.

\section{Gruppo di rinormalizzazione}
\subsection{Real-space renormalization group}
Veniamo ora a uno dei modi più pratici per studiare le transizioni di fase sui frattali, ovverosia il gruppo di rinormalizzazione.

L'idea dell'uso del gruppo di rinormalizzazione per studiare le transizioni di fase
è dettata dall'evidente invarianza per trasformazioni di scala del sistema in condizioni critiche.

Nel caso del modello di Ising una valida azione del gruppo di rinormalizzazione sul nostro grafo è
identificabile con l'operazione di \textit{blocking}. Questa consiste nel raggruppare gli spin in blocchi,
e costruire un nuovo grafo facendo corrispondere ad ogni blocco del vecchio grafo uno spin del nuovo,
e scegliendo il valore dello spin nel nuovo grafo secondo un voto a maggioranza tra gli spin del blocco del vecchio grafo.
Scegliamo poi condizioni di adiacenza fra i nuovi spin in base alle condizioni di adiacenza dei vecchi
blocchi.

Si può mostrare che l'operazione di blocking così definita preserva la funzione di partizione,
ma in generale non l'hamiltoniana
Ora il nostro sistema, prima dell'operazione di \textit{blocking}, era un campione
descrittivo di un'Hamiltoniana $\mathcal{H}:\Sigma\rightarrow\mathbb{R}$.
Successivamente all'operazione di \textit{blocking} il sistema è un campione
rappresentativo di un'Hamiltoniana $\mathcal{H'}:\Sigma\rightarrow\mathbb{R}$.
In generale un'Hamiltoniana è descritta da un insieme infinito (ma numerabile) di coefficienti,
ciascuno associato ad un diverso tipo di interazione. Formalmente stiamo dicendo che esiste
una bigezione tra l'insieme dei coefficienti e quello delle Hamiltoniane:
$\{K_i\}\leftrightarrow\mathcal{H}$. L'azione di blocking agisce sullo spazio dei coefficienti:
${K_i'} = \mathcal{R}\left(\{K_i\}\right)$. 

\slidesep

In condizioni critiche il nostro sistema è invariante sotto trasformazioni di scala.
Ovverosia ci aspettiamo che l'operazione di blocking agisca come l'identità sullo spazio dei coefficienti,
o ancora in altri termini, che il punto critico sia un punto fisso dell'azione del gruppo di rinormalizzazione.

Quindi, vediamo cosa succede in prossimità di un punto fisso ${K^*_i}$. Possiamo espandere linearmente l'azione del gruppo di 
rinormalizzazione e diagonalizzare questa operazione $M_{ij}$. 
Abbiamo quindi dei coefficienti $\lambda_i$ a partire dai quali definiamo gli 
\textit{autovalori del gruppo di rinormalizzazione} $y_i$.
Possiamo quindi fare distinzione tra \textit{variabili di scaling} $u_i$
rilevanti, marginali e irrilevanti.

Nel caso del modello di Ising abbiamo due parametri da regolare per trovare il punto critico,
la temperatura $T$ e il campo esterno $H$. Ci aspettiamo quindi due autovalori rilevanti, $y_t$ e $y_h$.

\slidesep

Possiamo poi ricondurci agli esponenti critici usuali $\nu$ e $\theta$.

A partire dalle loro definizioni e dallo scaling della funzione di correlazione, 
possiamo ottenere il legame tra gli esponenti critici e i due autovalori del gruppo di rinormalizzazione $y_t$ e $y_h$.

\begin{align*}
	\nu &= 1/y_t \\
	\theta &= 1/y_h
\end{align*}

\subsection{Relazioni di \textit{scaling}}

Senza perdita di generalità, si possono ottenere a partire dallo scaling della parte singolare dell'energia libera,
le relazioni di scaling fra gli esponenti critici, che mettono in relazione gli esponenti critici stessi con la dimensione del sistema $d$:

---

\subsection{MCRG}
%8.4.1 - 8.4.2 \hspace{10pt} Newmann - Barkema

Vediamo ora come misurare praticamente gli autovalori del gruppo di rinormalizzazione $y_t$ e $y_h$.

Possiamo porci in un intorno del punto critico ${K^*_i}$, 
e scegliere di lavorare con le variabili di scaling $u_i$ al posto dei coefficienti $K_i$.

Consideriamo allora i nostri sistemi e le nostre hamiltoniane come descritte dalle sole variabili di scaling rilevanti, 
approssimando quelle irrilevanti a 0.

Operazioni di blocking (ripetute)
ci portano in una nuova hamiltoniana e in un nuovo sistema primato, che però, modulo riscalare le variabili di scaling 
rilevanti è lo stesso di partenza.

Se per semplicità poniamo il campo magnetico esterno a 0,
la nostra variabile di scaling rilevante può essere identificata con la sola temperatura ridotta $t$.

Possiamo poi approssimare le hamiltoniane e troncarle 
tenendo solo i termini più rilevanti.
Ad esempio possiamo prendere solo l'interazione fra primi vicini. 

Il coefficiente di accoppiamento $K_1$ può essere riscritto in termini delle variabili di scaling. Vorremmo quindi misurare $K_1$ o $t$ prima e dopo l'operazione di blocking,
ma al lato pratico possiamo solo fare qualcosa di indiretto,
sfruttando magari una funzione della temperatura, come l'energia interna.

Chiediamo allora che l'energia interna del nostro sistema costruito con l'operazione di
blocking a partire da una temperatura $t$ sia uguale all'energia
interna di un sistema costruito a partire da una temperatura $t'$.

\slidesep
Se con l'operazione di blocking riscaliamo il nostro sistema di un fattore lineare $b$ ci aspettiamo
che anche la lunghezza di correlazione $\xi$ sia riscalata a $\xi'=\xi/b$. Usando la definizione di $\nu$:
$\xi \propto |t|^{-\nu}$ con $t=\frac{T-T_c}{T_c}$, si ha che:
\[\nu = \frac{\log b}{\log\left(\frac{\dd u'}{\dd T}\Big/\frac{\dd u}{\dd T}\right)}\]

%\subsection{OPE e $\epsilon$-expansion}
%\href{https://renormalwords.wordpress.com/2016/02/27/the-epsilon-expansion-and-the-ising-model/}{Qui} 
%è ben condensato, ma mi dicono sia banale, quindi meglio skippare.
%\subsection{Inverse renormalization group}
%\href{http://www.wisdom.weizmann.ac.il/~achi/LOP124.pdf}{Parlare di quello che fanno in questo paper}.

%\section{Preservare le traslazioni}
%\subsection{Con OPE e $\epsilon$-expansion}
%OPE
%\subsection{Altri strumenti della CFT}
%CFT

\section{Frattali}
\subsection{Condizioni sul punto critico}
Nello studio dei fenomeni critici sui frattali una prima domanda a cui poter dare una risposta è:
quale condizione è necessaria e/o sufficiente per avere una transizione di fase ad una temperatura
critica $T_c > 0$? 

\href{https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.45.855}
{QUA UN BEL PAPER, anche se da qualche parte ha cose un po' rotte, 
ma vengono aggiustate nei \textit{sequel}.}

Per rispondere a questa domanda, definiamo\footnote{Riprendendo la terminologia da questi 3 paper:
\href{https://iopscience.iop.org/article/10.1088/0305-4470/16/6/021}
{Y Gefen et al 1983 J. Phys. A: Math. Gen. 16 1267},
\href{https://iopscience.iop.org/article/10.1088/0305-4470/17/2/028}
{Y Gefen et al 1984 J. Phys. A: Math. Gen. 17 435},
\href{https://iopscience.iop.org/article/10.1088/0305-4470/17/6/024}
{Y Gefen et al 1984 J. Phys. A: Math. Gen. 17 1277} e da
\href{http://libgen.lc/ads.php?md5=79423B5CBAD001D7A0E4A11AB6691368}
{Mandelbrot, 1982,  The fractal geometry of nature}.} 
l'\textit{ordine di ramificazione} $\mathcal{R}_P$ di un punto $P$ di un frattale 
come il minimo numero di punti da rimuovere per separare un intorno di $P$ dal resto 
del frattale. In generale abbiamo quindi un valore massimo e uno minimo di $\mathcal{R}$,
in funzione del punto $P$ che scegliamo.
Urysohn ha dimostrato\footnote{Mandelbrot, 1982,  The fractal geometry of nature, pagina 137} 
che questi due sono legati dalla disuguaglianza 
$\mathcal{R}_\mathrm{max} \ge 2\mathcal{R}_\mathrm{min}-2$.

Una condizione necessaria e sufficiente per impedire che 
un grafo definito a partire da un frattale abbia una transizione di fase ad una temperatura 
$T_c > 0$ è che l'ordine di ramificazione minimo del frattale sia finito. 

\slidesep

Ciò è dettato da un ragionamento analogo all'argomento di Peierls. In condizioni di equilibrio a 
temperatura e volume fissati, il nostro modello di Ising cercherà una configurazione che minimizzi
l'energia libera di Helmholtz, con le dovute fluttuazioni di energia. Il punto è: queste fluttuazioni 
di energia sono sufficienti a creare una \textit{goccia} infinita? In un frattale infinito, 
preso un qualunque punto e un qualunque suo intorno $\mathcal{I}_P$, 
la cardinalità $|\mathcal{I}_P|$ è infinita. Ma se scegliamo l'intorno che minimizza i punti da rimuovere
per separare $P$ dal resto del frattale, otteniamo una \textit{goccia} infinita con un bordo finito, e 
quindi con una fluttuazione di energia finita. Pertanto la fluttuazione \textit{non è soppressa} e può
accadere. (scrivere meglio, sono le 10 di sera non ho testa).

Esempi di grafi con ordine di ramificazione finito che non presentano quindi una transizione di fase per 
$T_c > 0$ sono dati dagli alberi e dal triangolo di Sierpinksi, 
mentre frattali con ordine di ramificazione infinita sono i tappeti di Sierpinski,
le spugne di Menger e le loro generalizzazioni.

\slidesep

Una conseguenza affascinante di questa condizione è che è possibile costruire un grafo con dimensione
$d_S$ arbitrariamente alta pur non avendo alcuna transizione di fase per $T > 0$.
È altrettanto possibile costruire un grafo con dimensione arbitrariamente prossima a 1, pur avendo
sempre una transizione di fase con temperatura critica $T_c > 0$.

\subsection{Tappeti di Sierpinski}
Come abbiamo detto, i tappeti di Sierpinski e le loro generalizzazioni in spazi ambienti più grandi sono
un classico esempio di frattali che esibiscono una transizione di fase a una temperatura $T_c$ maggiore 
di 0. Inoltre, il grafo generato dai tappeti di Sierpinski ha una distanza fortemente equivalente con 
le distanze $p-$esime dello spazio ambiente. Pertanto, la dimensione di scala $d_S$ è uguale alla
dimensione del frattale $d_H$.

\slidesep

Prima di proseguire, vale la pena soffermarsi a notare come, mentre in dimensione intera bastino 
la dimensione $d$, la simmetria di scala e quella traslazionale per definire completamente lo spazio, 
nel caso dei frattali il generatore contiene più informazione della semplice dimensione $d_H$.

Sono quindi solitamente definiti in letteratura altri parametri, descrittivi del frattale, come la 
\textit{connettività} $\mathcal{Q}$ e la \textit{lacunarietà} $\mathcal{L}$.

CONNETTIVITÀ: Secondo la definizione di Mandelbrot\footnote{Terzo paper}, la minima dimensione della
frontiera di un intorno di un punto.

LACUNARIETÀ: Varie definizioni, fondamentalmente cercano di misurare quantitavamente la larghezza media 
dei buchi del frattale e fornire una misura di quanto il frattale fallisca nell'essere
invariante sotto traslazioni. In terzo cercano di dire $\mathcal L \simeq \frac1M\sum_{i=1}^M (n_i - \bar n)$.

\slidesep

Diversi lavori hanno mostrato queste transizioni di fase\footnote{Random selection: 
\href{https://arxiv.org/pdf/cond-mat/9802018.pdf}{1},
\href{https://sci-hub.se/10.1103/PhysRevB.96.174407}{2},
\href{https://www.sciencedirect.com/science/article/pii/S0378437108008789}{3},
\href{https://www.researchgate.net/publication/235512700_Magnetic_critical_behavior_of_fractals_in_dimensions_between_2_and_3}{4},
}.
Ho scelto di provare anch'io a analizzare numericamente queste transizioni di fase dei tappeti di 
Sierpinski e delle loro generalizzazioni (spugne di Menger etc.). Per farlo, ho scritto un po' di codice
per generare dei tappeti di Sierpinski a partire da alcuni parametri, ovvero la dimensione ambiente
$d_\mathrm{env}$, il lato del generatore $b$ e il numero di cubetti extra da selezionare $e$.

A questo punto ho potuto usare gli strumenti del Monte Carlo Renormalization Group e del Monte Carlo FSS 
per ricavare gli esponenti critici.

\subsection{Risultati delle simulazioni}

TABELLE, GRAFICI: DISCUTERE I PATTERN OSSERVATI E POCHE CHIACCHIERE:
\begin{itemize}
	\item Dimensionalità e temperatura critica
	\item Connettività e temperatura critica
	\item Lacunarietà e temperatura critica
	\item D, Q, L e esponenti critici
	\item Renormalization flows dal terzo.
\end{itemize}

% Importante! Metti le mie, quelle degli altri 
\subsection{Universalità e confronto con $\epsilon$-expansion}
Come mostrato, gli esponenti critici dipendono non dai coefficienti di accoppiamento ma solo dai parametri
di \textit{struttura} del nostro sistema, quali la dimensione del grafo $d_S$, ma anche da altri parametri
, come l'ordine di ramificazione, la connettività e la lacunarità del frattale
\footnote{Vedi \href{https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.45.855}{questo paper}}. 
Nel caso in cui però il nostro sistema esibisca anche una simmetria traslazionale, 
\textit{sembra} sufficiente la dimensione del grafo per
definire la classe di universalità del nostro modello e quindi gli esponenti critici.

In particolare le simulazioni sui frattali mostrano risultati non compatibili 
con quelli ottenuti da diversi metodi di CFT, come la
$\varepsilon-$expansion e il conformal bootstrap, che assumono l'invarianza traslazionale.

Sarebbe quindi fantasmagorico riuscire a costruire un grafo che mantenga la simmetria di traslazione
e abbia dimensione non intera. Ma sembra una sfida tosta questa.

\subsection{Frattali invarianti sotto traslazioni}
Qualcosa di bello estratto da 375 del Mandelbrot. Eccolo:

``Given the definitions of lacunarity in Chapter
34, a nonlacunar set in the space $\mathbb{R}^E$ should
intersect every cube or sphere in said space.
In mathematical terms, it should be 
everywhere dense, hence nonclosed. (The only 
everywhere dense closed set is $\mathbb{R}^E$ itself!) This
entry shows that such fractals do exist, but
"feel" very different from the closed fractals
in the rest of this Essay. A key symptom is
that the Hausdorff Besicovitch dimension 
remains workable, but the similarity and 
Minkowski Bouligand dimensions are equal to $E$,
rather than to the Hausdorff Besicovitch $D$.''

\subsection{Relazioni di scaling}
Sparare i grafici, dire "è bello torna, come ci aspettavamo", fantastico.




\end{document}
